# LB1.XMLXSD project

Examples



## Generating JAXB classes with maven

in terminal run the maven goal: `mvn compile`

##### Eclipse

Create "Run Configuration"
- Run > Run Configurations...
- In the search box type: *maven*
- Double-click on *Maven*
- Select your client project in the *Base directory*
- In the Goal field type: *compile*
- Press: *Run*  
**Note**: Clean the target directory before generating the artifact.

##### Intellij Idea

In the *Maven* view:
- Run: *Lifecicle > compile*

## Testing XSLT
Due to modern browsers refuse load xslt from filesystem (CORS violation), 
it's recommended to test this part of assigment in Internet Explorer browser
Here implemented filtering feature, so content of each group is hidden under 
*spoiler*.
