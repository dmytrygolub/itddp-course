<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sn="http://it.nure.ua/social">
    <xsl:output method="html" encoding="UTF-8"/>

    <xsl:template match="/sn:posts">
        <html>
            <head>
                <title>Posts</title>
                <link rel="stylesheet" type="text/css" href="style.css" />
            </head>
            <body>
                <div class="posts">
                    <div>
                        <label for="toggler-id-1">Public posts [Click to expand]</label>
                        <input type="checkbox" id="toggler-id-1" class="toggler" />
                        <div class="toggler-content">
                            <xsl:apply-templates select="sn:post[sn:access_level='PUBLIC']"/>
                        </div>
                    </div>
                    <div>
                        <label for="toggler-id-2">Posts available for friends [Click to expand]</label>
                        <input type="checkbox" id="toggler-id-2" class="toggler" />
                        <div class="toggler-content">
                            <xsl:apply-templates select="sn:post[sn:access_level='FRIENDS_ONLY']"/>
                        </div>
                    </div>
                    <div>
                        <label for="toggler-id-3">Private posts [Click to expand]</label>
                        <input type="checkbox" id="toggler-id-3" class="toggler" />
                        <div class="toggler-content">
                            <xsl:apply-templates select="sn:post[sn:access_level='HIDDEN']"/>
                        </div>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>

    <!-- Match 'sn:post' elements -->
    <xsl:template match="sn:post">
        <div class="post">
            <h1><xsl:value-of select="sn:header"/></h1>
            <p><xsl:value-of select="sn:text"/></p>
            <div class="images">
                <xsl:apply-templates select="sn:images/sn:image"/>
            </div>
            <xsl:call-template name="posted-by"/>
            <p>Date: <xsl:value-of select="sn:date"/></p>
            <i><xsl:apply-templates select="sn:access_level"/></i>
            <h3>Likes: <xsl:value-of select="sn:likes"/></h3>
            <h2>Comments:</h2>
            <div class="comments">
                <xsl:apply-templates select="sn:comments/sn:comment"/>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="sn:image">
        <xsl:choose>
            <xsl:when test="sn:image_url">
                <img src="{sn:image_url}"/>
            </xsl:when>
            <xsl:when test="sn:image_content">
                <img src="{sn:image_content}"/>
            </xsl:when>
            <xsl:otherwise>
                <p>No image.</p>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="posted-by">
        <div class="posted-by">
            <span class="user-image posted-by-item">
                <xsl:apply-templates select="sn:created_by/sn:image"/>
            </span>
            <span class="user-info posted-by-item">
                <i>Posted by: <a href="/{sn:created_by/sn:username}">
                    <xsl:value-of select="sn:created_by/sn:username"/></a></i>
            </span>
        </div>
    </xsl:template>

    <xsl:template match="sn:comment">
        <div class="comment">
            <b><xsl:value-of select="sn:text"/></b>
            <xsl:call-template name="posted-by"/>
            <p>Date: <xsl:value-of select="sn:date"/></p>
        </div>
    </xsl:template>

    <xsl:template match="sn:access_level">
        <xsl:choose>
            <xsl:when test=".='HIDDEN'">Content is hidden</xsl:when>
            <xsl:when test=".='FRIENDS_ONLY'">Content available only for friends</xsl:when>
            <xsl:when test=".='PUBLIC'">Content is publicly visible!</xsl:when>
            <xsl:otherwise>Unknown access level</xsl:otherwise> <!-- Handle other cases if needed -->
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
